<?php
/**
*云猫
*QQ3522934828
*/
session_start();
// 设置下载链接的有效期（10分钟）
$expireTime = 10 * 60; // 10分钟
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['captcha'])) {
    if ($_POST['captcha'] ===$_SESSION['captcha']) {
        $_SESSION['download_token'] = bin2hex(random_bytes(32));
        $_SESSION['download_token_time'] = time();
        header('Location: download.php?token=' . $_SESSION['download_token']);
        exit;
    } else {
        echo '验证码错误';
    }
} elseif (isset($_GET['token']) && isset($_SESSION['download_token']) && $_GET['token'] ===$_SESSION['download_token']) {
    if (isset($_SESSION['download_token_time']) && time() -$_SESSION['download_token_time'] < $expireTime) {
        $file = 'example.zip'; 
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        } else {
            echo '文件不存在';
        }
    } else {
        echo '您的下载链接已过期，请重新获取。';
    }
} else {
    echo '无效的下载请求';
}
?>